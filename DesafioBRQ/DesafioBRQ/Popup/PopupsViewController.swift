//
//  PopupsViewController.swift
//  DesafioBRQ
//
//  Created by Diogo Menezes on 17/08/2018.
//  Copyright © 2018 Diogo Menezes. All rights reserved.
//

import UIKit

protocol PopupDelegate {
    func popupStatus()
}

class PopupsViewController: UIViewController {
    
    @IBOutlet weak var viewPopup: UIView!
    @IBOutlet weak var titlePopup: UILabel!
    @IBOutlet weak var descriptionPopup: UILabel!
    @IBOutlet weak var buttonPopup: UIButton!
    
    var popupTitle:String?
    var messagePopup:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startValuesComponents()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    var delegate:PopupDelegate?
    
    @IBAction func buttonPopupConfirmPurchase(_ sender: Any) {
        self.delegate?.popupStatus()
    }
    
    func startValuesComponents(){
        if let title = popupTitle {
            titlePopup.text = title.uppercased()
        }
        if let description = messagePopup {
            descriptionPopup.text = description
        }
    }
    
}

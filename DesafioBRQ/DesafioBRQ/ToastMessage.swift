//
//  ToastMessage.swift
//  teste
//
//  Created by Andrew Castro on 17/08/2018.
//  Copyright © 2018 Diogo Menezes. All rights reserved.
//

import Foundation
import UIKit

class ToastMessage {

class func showHUDClipboard () {
    var hud = MBProgressHUD.showAdded(to: navigationController?.view, animated: true)
    // Configure for text only and offset down
    hud.mode = MBProgressHUDModeText
    hud.label.text = "Some message..."
    hud.margin = 10.0
    hud.yOffset = 150.0
    hud.removeFromSuperViewOnHide = true
    hud.hide(animated: true, afterDelay: 3)
}

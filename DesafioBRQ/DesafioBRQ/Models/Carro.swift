//
//  Carro.swift
//  DesafioBRQ
//
//  Created by  Diogo Menezes on 8/16/18.
//  Copyright © 2018 Diogo Menezes. All rights reserved.
//

import Foundation

struct Carros: Codable {
    let id: Int
    let nome: String
    let descricao:String
    let preco: Float
    let marca:String
    let quantidade:Int
    let imagem: String
    
    init(id:Int, nome:String, descricao:String, preco:Float, marca:String, quantidade:Int, imagem:String) {
        self.id = id
        self.nome = nome
        self.descricao = descricao
        self.preco = preco
        self.marca = marca
        self.quantidade = quantidade
        self.imagem = imagem
    }
    
}

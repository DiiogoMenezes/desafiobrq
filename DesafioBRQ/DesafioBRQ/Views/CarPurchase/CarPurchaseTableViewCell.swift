//
//  CarPurchaseTableViewCell.swift
//  DesafioBRQ
//
//  Created by Diogo Menezes on 18/08/2018.
//  Copyright © 2018 Diogo Menezes. All rights reserved.
//

import UIKit

class CarPurchaseTableViewCell: UITableViewCell {

    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var nameCarCell: UILabel!
    @IBOutlet weak var priceCarCell: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

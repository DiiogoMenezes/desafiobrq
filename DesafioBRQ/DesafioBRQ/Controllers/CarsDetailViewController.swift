//
//  CarsDetailViewController.swift
//  DesafioBRQ
//
//  Created by Diogo Menezes on 17/08/2018.
//  Copyright © 2018 Diogo Menezes. All rights reserved.
//

import UIKit

class CarsDetailViewController: UIViewController {
    
    @IBOutlet weak var imageDetail: UIImageView!
    @IBOutlet weak var viewDetailDatas: UIView!
    @IBOutlet weak var textViewDescription: UITextView!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelQuantity: UILabel!
    @IBOutlet weak var buttonBuyCar: UIButton!
    
    var carros:Carros!
    var nomeCarroSet:String?
    var precoCarroSet:Float?
    var imageCarroSet:String?
    var countCars = 0
    var arrayData:Array<String> = []
    
    @IBAction func actionCarBuy(_ sender: Any) {
        self.nomeCarroSet = carros.nome
        self.precoCarroSet = carros.preco
        self.imageCarroSet = carros.imagem
        if countCars < 4 {
            self.countCars += 1
            self.arrayData.append(self.nomeCarroSet!)
        }
        self.verifyLimitPurchase()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getDetailsCar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if countCars < 4 {
            self.buttonBuyCar.isEnabled = true
            self.buttonBuyCar.backgroundColor = UIColor.blue
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let seguePurchase = segue.destination as? ShoppingCartViewController, segue.identifier == "carPurchaseSegue"{
            seguePurchase.name = self.nomeCarroSet
            seguePurchase.price = self.precoCarroSet
            seguePurchase.image = self.imageCarroSet
            seguePurchase.arrayDataCar = self.arrayData
            seguePurchase.countCarsPurchase = self.countCars
        }
    }
    
    func getDetailsCar(){
        if let url = URL(string: carros.imagem){
            DispatchQueue.global().async {
                if let data = try? Data(contentsOf: url){
                    DispatchQueue.main.async {
                        self.imageDetail.image = UIImage(data: data)
                    }
                }
            }
        }
        self.textViewDescription.text = carros.descricao
        self.labelPrice.text = "R$ \(String(carros.preco))"
        self.labelQuantity.text = "Qt: \(String(carros.quantidade))"
    }
    
    func toastMessage(message: String){
        let message = message
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        self.present(alert, animated: true)
        let duration: Double = 0.7
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
            alert.dismiss(animated: true)
        }
    }
    
    func verifyLimitPurchase(){
        if self.countCars < 4 {
            self.toastMessage(message: "Carro adicionado no carrinho!")
            self.buttonBuyCar.isEnabled = true
        }else{
            self.toastMessage(message: "Limite de compra atingido!")
            self.buttonBuyCar.backgroundColor = UIColor.gray
            self.buttonBuyCar.isEnabled = false
        }
    }
}

//
//  CarrosTableViewController.swift
//  DesafioBRQ
//
//  Created by Diogo Menezes on 8/16/18.
//  Copyright © 2018 Diogo Menezes. All rights reserved.
//

import UIKit

class CarrosTableViewController: UITableViewController {

    var carro:Carros?
    var carros : [Carros] = []
    var carDetail:Carros?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadCars()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func loadCars() {
        let fileURL = Bundle.main.url(forResource: "cars.json", withExtension: nil)!
        let jsonData = try! Data(contentsOf: fileURL)
        do {
            carros = try JSONDecoder().decode([Carros].self, from: jsonData)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    // MARK: - Table view data source

    /*
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
     */
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return carros.count
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let controller = segue.destination as! CarsDetailViewController
        self.carDetail = carros[tableView.indexPathForSelectedRow!.row]
        controller.carros = carDetail
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CarrosTableViewCell
        
        carro = carros[indexPath.row]
        
        if let url = URL(string: (carro?.imagem)!){
            DispatchQueue.global().async {
                if let data = try? Data(contentsOf: url){
                    DispatchQueue.main.async {
                        cell.carsImage.image = UIImage(data: data)
                    }
                }
            }
        }
        if let priceCar = carro?.preco{
            cell.labelPrice.text = "\(priceCar)"
        }
        
        cell.labelName.text = carro?.nome
        cell.labelCarBrand.text = carro?.marca
        return cell
    }
}

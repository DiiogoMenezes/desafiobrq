//
//  CarrosTableViewCell.swift
//  DesafioBRQ
//
//  Created by  Diogo Menezes on 8/16/18.
//  Copyright © 2018 Diogo Menezes. All rights reserved.
//

import UIKit

class CarrosTableViewCell: UITableViewCell {
    
    @IBOutlet weak var carsImage: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelCarBrand: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

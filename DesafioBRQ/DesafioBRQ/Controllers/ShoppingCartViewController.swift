//
//  ShoppingCartViewController.swift
//  DesafioBRQ
//
//  Created by Diogo Menezes on 17/08/2018.
//  Copyright © 2018 Diogo Menezes. All rights reserved.
//

import UIKit


class ShoppingCartViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, PopupDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var labelShoppingCart: UILabel!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var tableViewCars: UITableView!
    @IBOutlet weak var buttonFinalizePurchase: UIButton!

    var dismissScreenListSuccess:Bool = false
    var totalPriceCart:Float = 0.0
    var image:String?
    var name:String?
    var price:Float?
    var countCarsPurchase:Int?
    var arrayDataCar:Array<String> = []
    
    @IBAction func finalizePurchase(_ sender: Any) {
        if self.tableViewCars.visibleCells.isEmpty{
            self.popupError()
        }else{
            self.popupSuccess()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.totalPrice.text = "Total do carrinho: \(self.totalPriceCart)"
        tableViewCars.dataSource = self
        tableViewCars.delegate = self
        tableViewCars.register(UINib(nibName: "CarPurchaseTableViewCell", bundle: nil), forCellReuseIdentifier: "carPurchase")
        navigationController?.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if self.dismissScreenListSuccess{
            let viewControllers = self.navigationController!.viewControllers as [UIViewController]
            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if let controller = viewController as? CarsDetailViewController {
            controller.countCars = countCarsPurchase!
            controller.arrayData = arrayDataCar
        }
    }
    
    func popupSuccess(){
        let controller = UIStoryboard(name: "Popups", bundle: nil).instantiateViewController(withIdentifier: "popupPurchase") as! PopupsViewController
        controller.delegate = self
        controller.popupTitle = "SUCESSO"
        controller.messagePopup = "Compra finalizada com sucesso. Obrigado!"
        self.dismissScreenListSuccess = true
        self.present(controller, animated: true, completion: nil)
    }
    
    func popupError(){
        let controller = UIStoryboard(name: "Popups", bundle: nil).instantiateViewController(withIdentifier: "popupPurchase") as! PopupsViewController
        controller.delegate = self
        controller.popupTitle = "ERRO"
        controller.messagePopup = "Não há nenhum carro na lista do carrinho!!"
        self.present(controller, animated: true, completion: nil)
    }
    
    func popupStatus() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayDataCar.count 
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.createCell(tableView, indexPath: indexPath)
    }
    
    func createCell(_ tableView:UITableView, indexPath:IndexPath) -> UITableViewCell {
        if let _ = self.name, let priceCar = self.price {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "carPurchase") as? CarPurchaseTableViewCell{
                cell.nameCarCell.text = arrayDataCar[indexPath.item]
                cell.priceCarCell.text = "\(priceCar)"
                if let imageSet = URL(string: (self.image)!){
                    DispatchQueue.global().async {
                        if let data = try? Data(contentsOf: imageSet){
                            DispatchQueue.main.async {
                                cell.imageCell.image = UIImage(data: data)
                            }
                        }
                    }
                }
                self.totalPriceCart += priceCar
                self.totalPrice.text = "Total do carrinho: \(self.totalPriceCart)"
                if self.countCarsPurchase! > 0 {
                    self.tableViewCars.isScrollEnabled = false
                }
                return cell
            }
        }
        return UITableViewCell.init()
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            self.arrayDataCar.remove(at: indexPath.item)
            tableView.deleteRows(at: [indexPath], with: .bottom)
            self.totalPriceCart = self.totalPriceCart - self.price!
            self.countCarsPurchase = self.countCarsPurchase! - 1
            self.totalPrice.text = "Total do carrinho: \(self.totalPriceCart)"
        }
    }

}

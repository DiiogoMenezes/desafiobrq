//
//  UserDefault.swift
//  teste
//
//  Created by Diogo Menezes on 18/08/2018.
//  Copyright © 2018 Diogo Menezes. All rights reserved.
//

import Foundation

open class UserDefault {
    
    class func setDataKey(_ value: String, key: String){
        
        let defaults = UserDefaults.standard
        defaults.setValue(value, forKey: key)
        
        UserDefaults.standard.synchronize();
        
    }
}
